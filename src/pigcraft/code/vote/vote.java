//Coded by Dev Ojha
package pigcraft.code.vote;

import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public final class Vote extends JavaPlugin {

	public Logger logger = this.getLogger();
	public static Vote plugin;
	
	@Override
	public void onDisable()
	{
		PluginDescriptionFile pdfile = this.getDescription();
		this.logger.info(pdfile.getName() + " has been disabled");
	}
	
	@Override
	public void onEnable()
	{
		PluginDescriptionFile pdfile = this.getDescription();
		this.logger.info(pdfile.getName() + " Version: " + pdfile.getVersion() + " has been Enabled");
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		Player player = (Player) sender;

		if(commandLabel.equalsIgnoreCase("vote")){
			player.sendMessage(ChatColor.GOLD + "Click This Link to Vote! http://tinyurl.com/cnqyk65");
			
		}
		return false;
	}
}

